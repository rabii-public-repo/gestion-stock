package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Article;
import com.gestion.stock.entities.Category;
import com.gestion.stock.service.IArticleService;
import com.gestion.stock.service.ICategoryService;

@Controller
@RequestMapping("/articles")
public class ArticleController {
	private static final Logger log = LoggerFactory.getLogger(ArticleController.class);

	@Autowired
	private IArticleService articleServ;
	@Autowired
	private ICategoryService catServ;

	@RequestMapping
	public String articles(Model model) {
		List<Article> articles = articleServ.selectAll();
		if (articles.isEmpty()) {
			articles = new ArrayList<Article>();
		}
		model.addAttribute("articlesList", articles);
		return "article/article";
	}

	@RequestMapping(params = "id")
	public String getOneArticle(Model model, @RequestParam long id) {
		Article article = articleServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("article", article);
		return "redirect:/articles";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		List<Category> categories = catServ.selectAll();
		if (categories == null) {
			categories = new ArrayList<Category>();
		}
		model.addAttribute("categories", categories);
		model.addAttribute("article", new Article());
		return "article/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Article article) {
		log.info("le article ajout� est {} ", article);
		articleServ.save(article);
		return "redirect:/articles";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Article article = articleServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le article maj est {} ", article);
		List<Category> categories = catServ.selectAll();
		if (categories == null) {
			categories = new ArrayList<Category>();
		}
		model.addAttribute("categories", categories);
		model.addAttribute("article", article);
		return "article/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Article article) {
		System.out.println(article.getIdArticle());
		articleServ.update(article); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/articles";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		articleServ.delete(id);
		return "redirect:/articles";
	}
}
