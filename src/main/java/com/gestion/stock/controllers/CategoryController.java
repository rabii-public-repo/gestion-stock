package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Category;
import com.gestion.stock.service.ICategoryService;

@Controller
@RequestMapping("/categories")
public class CategoryController {
	private static final Logger log = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private ICategoryService categoryServ;

	@RequestMapping
	public String categories(Model model) {
		List<Category> categories = categoryServ.selectAll();
		if (categories.isEmpty()) {
			categories = new ArrayList<Category>();
		}
		model.addAttribute("categoriesList", categories);
		return "category/category";
	}

	@RequestMapping(params = "id")
	public String getOneCategory(Model model, @RequestParam long id) {
		Category category = categoryServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("category", category);
		return "redirect:/categories";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("category", new Category());
		return "category/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Category category) {
		log.info("le category ajout� est {} ", category);
		categoryServ.save(category);
		return "redirect:/categories";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Category category = categoryServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le category maj est {} ", category);
		model.addAttribute("category", category);
		return "category/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Category category) {
		System.out.println(category.getIdCategory());
		categoryServ.update(category); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/categories";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		categoryServ.delete(id);
		return "redirect:/categories";
	}
}
