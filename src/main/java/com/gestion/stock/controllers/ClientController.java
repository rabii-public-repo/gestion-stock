package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Client;
import com.gestion.stock.service.IClientService;

@Controller
@RequestMapping("/clients")
public class ClientController {
	private static final Logger log = LoggerFactory.getLogger(ClientController.class);

	@Autowired
	private IClientService clientServ;

	@RequestMapping
	public String clients(Model model) {
		List<Client> clients = clientServ.selectAll();
		if (clients.isEmpty()) {
			clients = new ArrayList<Client>();
		}
		model.addAttribute("clientsList", clients);
		return "client/client";
	}

	@RequestMapping(params = "id")
	public String getOneClient(Model model, @RequestParam long id) {
		Client client = clientServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("client", client);
		return "redirect:/clients";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("client", new Client());
		return "client/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Client client) {
		log.info("le client ajout� est {} ", client);
		clientServ.save(client);
		return "redirect:/clients";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Client client = clientServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le client maj est {} ", client);
		model.addAttribute("client", client);
		return "client/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Client client) {
		System.out.println(client.getIdClient());
		clientServ.update(client); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/clients";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		clientServ.delete(id);
		return "redirect:/clients";
	}
}
