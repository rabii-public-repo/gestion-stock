package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Fournisseur;
import com.gestion.stock.service.IFournisseurService;

@Controller
@RequestMapping("/fournisseurs")
public class FournisseurController {
	private static final Logger log = LoggerFactory.getLogger(FournisseurController.class);

	@Autowired
	private IFournisseurService fournisseurServ;

	@RequestMapping
	public String fournisseurs(Model model) {
		List<Fournisseur> fournisseurs = fournisseurServ.selectAll();
		if (fournisseurs.isEmpty()) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		model.addAttribute("fournisseursList", fournisseurs);
		return "fournisseur/fournisseur";
	}

	@RequestMapping(params = "id")
	public String getOneFournisseur(Model model, @RequestParam long id) {
		Fournisseur fournisseur = fournisseurServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("fournisseur", fournisseur);
		return "redirect:/fournisseurs";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("fournisseur", new Fournisseur());
		return "fournisseur/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Fournisseur fournisseur) {
		log.info("le fournisseur ajout� est {} ", fournisseur);
		fournisseurServ.save(fournisseur);
		return "redirect:/fournisseurs";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Fournisseur fournisseur = fournisseurServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le fournisseur maj est {} ", fournisseur);
		model.addAttribute("fournisseur", fournisseur);
		return "fournisseur/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Fournisseur fournisseur) {
		System.out.println(fournisseur.getIdFournisseur());
		fournisseurServ.update(fournisseur); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/fournisseurs";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		fournisseurServ.delete(id);
		return "redirect:/fournisseurs";
	}
}
