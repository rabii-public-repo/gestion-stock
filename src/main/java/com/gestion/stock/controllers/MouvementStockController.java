package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.MouvementStock;
import com.gestion.stock.service.IMouvementStockService;

@Controller
@RequestMapping("/mouvementStocks")
public class MouvementStockController {
	private static final Logger log = LoggerFactory.getLogger(MouvementStockController.class);

	@Autowired
	private IMouvementStockService mvtStkServ;

	@RequestMapping
	public String mouvementStocks(Model model) {
		List<MouvementStock> mouvementStocks = mvtStkServ.selectAll();
		if (mouvementStocks.isEmpty()) {
			mouvementStocks = new ArrayList<MouvementStock>();
		}
		model.addAttribute("mouvementStocksList", mouvementStocks);
		return "mouvementStock/mouvementStock";
	}

	@RequestMapping(params = "id")
	public String getOneMouvementStock(Model model, @RequestParam long id) {
		MouvementStock mouvementStock = mvtStkServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("mouvementStock", mouvementStock);
		return "redirect:/mouvementStocks";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("mouvementStock", new MouvementStock());
		return "mouvementStock/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(MouvementStock mouvementStock) {
		log.info("le mouvementStock ajout� est {} ", mouvementStock);
		mvtStkServ.save(mouvementStock);
		return "redirect:/mouvementStocks";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		MouvementStock mouvementStock = mvtStkServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le mouvementStock maj est {} ", mouvementStock);
		model.addAttribute("mouvementStock", mouvementStock);
		return "mouvementStock/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(MouvementStock mouvementStock) {
		System.out.println(mouvementStock.getIdMvtStck());
		mvtStkServ.update(mouvementStock); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/mouvementStocks";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		mvtStkServ.delete(id);
		return "redirect:/mouvementStocks";
	}
}
