package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Utilisateur;
import com.gestion.stock.service.IUtilisateurService;

@Controller
@RequestMapping("/utilisateurs")
public class UtilisateurController {
	private static final Logger log = LoggerFactory.getLogger(UtilisateurController.class);

	@Autowired
	private IUtilisateurService utilisateurServ;

	@RequestMapping
	public String utilisateurs(Model model) {
		List<Utilisateur> utilisateurs = utilisateurServ.selectAll();
		if (utilisateurs.isEmpty()) {
			utilisateurs = new ArrayList<Utilisateur>();
		}
		model.addAttribute("utilisateursList", utilisateurs);
		return "utilisateur/utilisateur";
	}

	@RequestMapping(params = "id")
	public String getOneUser(Model model, @RequestParam long id) {
		Utilisateur utilisateur = utilisateurServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("utilisateur", utilisateur);
		return "redirect:/utilisateurs";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("utilisateur", new Utilisateur());
		return "utilisateur/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Utilisateur utilisateur) {
		log.info("l'utilisateur ajout� est {} ", utilisateur);
		utilisateurServ.save(utilisateur);
		return "redirect:/utilisateurs";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Utilisateur utilisateur = utilisateurServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("l'utilisateur maj est {} ", utilisateur);
		model.addAttribute("utilisateur", utilisateur);
		return "utilisateur/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Utilisateur utilisateur) {
		System.out.println(utilisateur.getIdUtilisateur());
		utilisateurServ.update(utilisateur); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/utilisateurs";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		utilisateurServ.delete(id);
		return "redirect:/utilisateurs";
	}
}
