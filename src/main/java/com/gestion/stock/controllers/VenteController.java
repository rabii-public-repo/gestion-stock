package com.gestion.stock.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gestion.stock.entities.Vente;
import com.gestion.stock.service.IVenteService;

@Controller
@RequestMapping("/ventes")
public class VenteController {
	private static final Logger log = LoggerFactory.getLogger(VenteController.class);

	@Autowired
	private IVenteService venteServ;

	@RequestMapping
	public String ventes(Model model) {
		List<Vente> ventes = venteServ.selectAll();
		if (ventes.isEmpty()) {
			ventes = new ArrayList<Vente>();
		}
		model.addAttribute("ventesList", ventes);
		return "vente/vente";
	}

	@RequestMapping(params = "id")
	public String getOneVente(Model model, @RequestParam long id) {
		Vente vente = venteServ.getById(id);// TODO gestion des exceptions dans la partie dao
		model.addAttribute("vente", vente);
		return "redirect:/ventes";
	}

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String displaySaveForm(Model model) {
		model.addAttribute("vente", new Vente());
		return "vente/new";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(Vente vente) {
		log.info("le vente ajout� est {} ", vente);
		vente.setDateVente(new Date());
		venteServ.save(vente);
		return "redirect:/ventes";
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET, params = "id")
	public String displayUpdateForm(Model model, @RequestParam long id) {
		Vente vente = venteServ.getById(id); // TODO gestion des exceptions au niveau de dao
		log.info("le vente maj est {} ", vente);
		model.addAttribute("vente", vente);
		return "vente/update";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Vente vente) {
		System.out.println(vente.getIdVente());
		venteServ.update(vente); // TODO : gestion des exceptions au niveau de dao
		return "redirect:/ventes";
	}

//TODO modifier url dans le jsp
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public String delete(@PathVariable long id) {
		System.out.println(id);
		venteServ.delete(id);
		return "redirect:/ventes";
	}
}
