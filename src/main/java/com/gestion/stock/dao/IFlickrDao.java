package com.gestion.stock.dao;

import java.io.InputStream;

public interface IFlickrDao {
	// cette methode permet d'enregistrer une image dans le serveur flickr et ensuite de retourner
	// url afin de le sauvegarder dans la base de donn�es
	public String savePhoto(InputStream stream, String title) throws Exception;

}
