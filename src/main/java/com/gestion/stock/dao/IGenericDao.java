package com.gestion.stock.dao;

import java.util.List;

/*
 * Je cr�e cette interface generique puis je cr�e son implementation
 * puis je cr�e des interfaces qui heritent de cette interface
 */
// TODO faire une exemple de test
public interface IGenericDao<E> {

	public E save(E entity);

	public E update(E entity);

	public List<E> selectAll();

	public List<E> selectAll(String sortField, String sort);

	public E getById(Long id);

	public E findOne(String paramName, Object paramValue);

	public E findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);
	
	public boolean delete(long id);

}
