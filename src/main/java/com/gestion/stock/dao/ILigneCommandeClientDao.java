package com.gestion.stock.dao;

import com.gestion.stock.entities.Article;
import com.gestion.stock.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{

}
