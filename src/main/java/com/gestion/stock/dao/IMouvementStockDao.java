package com.gestion.stock.dao;

import com.gestion.stock.entities.Article;
import com.gestion.stock.entities.MouvementStock;

public interface IMouvementStockDao extends IGenericDao<MouvementStock>{

}
