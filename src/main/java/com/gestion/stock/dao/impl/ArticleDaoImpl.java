package com.gestion.stock.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gestion.stock.dao.IArticleDao;
import com.gestion.stock.entities.Article;

public class ArticleDaoImpl extends GenericDaoImpl<Article> implements IArticleDao {
	private static final Logger log = LoggerFactory.getLogger(ArticleDaoImpl.class);

	public ArticleDaoImpl() {
		log.info("+++++    bean ArticleDaoImpl    +++++");
	}
}