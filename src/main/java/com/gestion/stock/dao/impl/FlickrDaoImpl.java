package com.gestion.stock.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.gestion.stock.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {
	private static final Logger log = LoggerFactory.getLogger(FlickrDaoImpl.class);
	private Flickr flickr;
	private static final UploadMetaData UPLOAD_META_DATA = new UploadMetaData();
	private static final String API_KEY = "b45e6306a70ed4375d402c6d85f44230";
	private static final String SHARED_SECRET = "6c51a33b300b5012";
	private static final String TOKEN = "";
	private static final String TOKEN_SECRET = "";
	private static final REST REST = new REST();

	// m�thode qui permet de se connecter au serveur flickr
	public void connect() {
		flickr = new Flickr(API_KEY, SHARED_SECRET, REST);
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken(TOKEN);
		auth.setTokenSecret(TOKEN_SECRET);
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

	// m�thode qui permet de uploader le photo sur le serveur flicker elle renvoi
	// url qu'on va enregistrer dans la base de donn�es
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		UPLOAD_META_DATA.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, UPLOAD_META_DATA);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}

	// methode authentification a utiliser une seule fois pour recuperer les code de
	// token et secret
	public void auth() {
		flickr = new Flickr(API_KEY, SHARED_SECRET, new REST());

		AuthInterface authInterface = flickr.getAuthInterface();

		Token token = new Token(API_KEY, SHARED_SECRET);
//		Token token = authInterface.getRequestToken();
		log.info("token : " + token);

		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorise yourself on flickr");
		System.out.println(url);
		System.out.println("Paste in the token it gives you :");
		System.out.println(">>");

		String tokenKey = JOptionPane.showInputDialog(null);

		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Authentification succes");

		Auth auth = null;
		try {
			auth = authInterface.checkToken(requestToken);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
		System.out.println("Token : " + requestToken.getToken());
		System.out.println("Secret : " + requestToken.getSecret());
		System.out.println("nsid : " + auth.getUser().getId());
		System.out.println("RealName : " + auth.getUser().getRealName());
		System.out.println("Username : " + auth.getUser().getUsername());

	}

}