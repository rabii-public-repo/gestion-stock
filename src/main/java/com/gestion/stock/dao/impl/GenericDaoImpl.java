package com.gestion.stock.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IGenericDao;
import com.gestion.stock.dao.util.DAOUtil;

@SuppressWarnings("unchecked")
@Service
public class GenericDaoImpl<E> implements IGenericDao<E> {

	@PersistenceContext
	private EntityManager em;

	private Class<E> type;

	public GenericDaoImpl() {
		type = (Class<E>) DAOUtil.getTypeArguments(GenericDaoImpl.class, this.getClass()).get(0);
	}

	public Class<E> getType() {
		return type;
	}

	@Override
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		em.merge(entity);
		return entity;
	}

	@Override
	public List<E> selectAll() {
		Query query = em.createQuery("from " + type.getSimpleName());
		// Query query = em.createQuery("select b from Client b");
		return query.getResultList();
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("FROM " + type.getSimpleName() + " order by " + sortField + " " + sort);
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {
		System.out.println(type.getClass());
		E entity = (E) em.find(type, id);
		// TODO gestion des exceptions
		return entity;
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		Query query = em.createQuery("FROM " + type.getSimpleName() + " WHERE " + paramName + " =" + paramValue);
		return (E) query.getSingleResult();
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramvalues) {
		return null;
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return 0;
	}

	@Override
	public boolean delete(long id) {
		E entity = (E) em.find(type, id);
		em.remove(entity);
		return entity == null;
	}
}
