package com.gestion.stock.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCreationBean {
	private static final Logger log = LoggerFactory.getLogger(TestCreationBean.class);

	public TestCreationBean() {
		log.info("+++++     TestCreationBean     +++++");
	}
}
