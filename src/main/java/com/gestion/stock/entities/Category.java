package com.gestion.stock.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class Category {

	private static final Logger log = LoggerFactory.getLogger(Category.class);
	@Id
	@SequenceGenerator(name = "category_seq", sequenceName = "category_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
	private Long idCategory;
	private String code;
	private String designation;
	@OneToMany(mappedBy = "category")
	private List<Article> articles;

	public Category() {
		log.info("+++++ bean category +++++");
	}

	public Long getIdArticle() {
		return idCategory;
	}

	public void setIdArticle(Long idArticle) {
		this.idCategory = idArticle;
	}

	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long idCategory) {
		this.idCategory = idCategory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
}
