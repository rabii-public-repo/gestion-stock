package com.gestion.stock.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "client")
public class Client {

	private static final Logger log = LoggerFactory.getLogger(Client.class);
	@Id
	@SequenceGenerator(name = "client_seq", sequenceName = "client_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_seq")
	private Long idClient;
	private String nom;
	private String prenom;
	private String email;
	private String adresse;
	private String photo;
	@OneToMany(mappedBy = "client")
	private List<CommandeClient> commandesClient;

	public Client() {
		log.info("+++++ bean client +++++");
	}

	public Long getIdArticle() {
		return idClient;
	}

	public void setIdArticle(Long idArticle) {
		this.idClient = idArticle;
	}

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<CommandeClient> getCommandesClient() {
		return commandesClient;
	}

	public void setCommandesClient(List<CommandeClient> commandesClient) {
		this.commandesClient = commandesClient;
	}

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
}
