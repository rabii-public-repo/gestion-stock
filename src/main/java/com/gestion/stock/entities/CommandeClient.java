package com.gestion.stock.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class CommandeClient {

	private static final Logger log = LoggerFactory.getLogger(CommandeClient.class);
	@Id
	@GeneratedValue
	private Long idCommandeClient;
	private String code;
	private Date dateCommande;
	@ManyToOne
	@JoinColumn(name = "idClient")
	private Client client;
	@OneToMany(mappedBy = "commandeClient")
	private List<LigneCommandeClient> lignesCommandesClient;

	public CommandeClient() {
		log.info("+++++ bean CommandeClient +++++");
	}

	public Long getIdArticle() {
		return idCommandeClient;
	}

	public void setIdArticle(Long idArticle) {
		this.idCommandeClient = idArticle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<LigneCommandeClient> getLignesCommandesClient() {
		return lignesCommandesClient;
	}

	public void setLignesCommandesClient(List<LigneCommandeClient> lignesCommandesClient) {
		this.lignesCommandesClient = lignesCommandesClient;
	}
}
