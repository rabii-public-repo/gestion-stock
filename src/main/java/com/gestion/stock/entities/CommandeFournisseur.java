package com.gestion.stock.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class CommandeFournisseur {

	private static final Logger log = LoggerFactory.getLogger(CommandeFournisseur.class);
	@Id
	@GeneratedValue
	private Long idCommandeFournisseur;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCmdFournisseur;
	
	@ManyToOne
	@JoinColumn(name = "idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy = "commandeFournisseur")
	private List<LigneCommandeFournisseur> lignesCmdFournisseur;

	public CommandeFournisseur() {
		log.info("+++++ bean CommandeFournisseur +++++");
	}

	public Long getIdArticle() {
		return idCommandeFournisseur;
	}

	public void setIdArticle(Long idArticle) {
		this.idCommandeFournisseur = idArticle;
	}

	public Long getIdFournisseur() {
		return idCommandeFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		this.idCommandeFournisseur = idFournisseur;
	}

	public Date getDateCmdFournisseur() {
		return dateCmdFournisseur;
	}

	public void setDateCmdFournisseur(Date dateCmdFournisseur) {
		this.dateCmdFournisseur = dateCmdFournisseur;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLignesCmdFournisseur() {
		return lignesCmdFournisseur;
	}

	public void setLignesCmdFournisseur(List<LigneCommandeFournisseur> lignesCmdFournisseur) {
		this.lignesCmdFournisseur = lignesCmdFournisseur;
	}
}
