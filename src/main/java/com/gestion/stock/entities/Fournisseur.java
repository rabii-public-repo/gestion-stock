package com.gestion.stock.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class Fournisseur {

	private static final Logger log = LoggerFactory.getLogger(Fournisseur.class);
	@Id
	@SequenceGenerator(name = "fournisseur_seq", sequenceName = "fournisseur_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fournisseur_seq")
	private Long idFournisseur;
	private String nom;
	private String prenom;
	private String adresse;
	private String photo;
	private String email;
	@OneToMany(mappedBy = "fournisseur")
	private List<CommandeFournisseur> commandesFournisseur;

	public Fournisseur() {
		log.info("+++++ bean fournisseur +++++");
	}

	public Long getIdArticle() {
		return idFournisseur;
	}

	public void setIdArticle(Long idArticle) {
		this.idFournisseur = idArticle;
	}

	public Long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<CommandeFournisseur> getCommandesFournisseur() {
		return commandesFournisseur;
	}

	public void setCommandesFournisseur(List<CommandeFournisseur> commandesFournisseur) {
		this.commandesFournisseur = commandesFournisseur;
	}
}
