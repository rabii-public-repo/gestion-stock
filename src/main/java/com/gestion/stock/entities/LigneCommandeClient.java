package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class LigneCommandeClient {

	private static final Logger log = LoggerFactory.getLogger(LigneCommandeClient.class);
	@Id
	@GeneratedValue
	private Long idLigneCmdClient;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCommandeClient")
	private CommandeClient commandeClient;

	public LigneCommandeClient() {
		log.info("+++++ bean LigneCommandeClient +++++");
	}

	public Long getIdArticle() {
		return idLigneCmdClient;
	}

	public void setIdArticle(Long idArticle) {
		this.idLigneCmdClient = idArticle;
	}

	public Long getIdLigneCmdClient() {
		return idLigneCmdClient;
	}

	public void setIdLigneCmdClient(Long idLigneCmdClient) {
		this.idLigneCmdClient = idLigneCmdClient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}
}
