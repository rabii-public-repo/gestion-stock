package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class LigneCommandeFournisseur {

	private static final Logger log = LoggerFactory.getLogger(LigneCommandeFournisseur.class);
	@Id
	@GeneratedValue
	private Long idLigneCmdFournisseur;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCmdFournisseur")
	private CommandeFournisseur commandeFournisseur;

	public LigneCommandeFournisseur() {
		log.info("+++++ bean LigneCommandeFournisseur +++++");
	}

	public Long getIdArticle() {
		return idLigneCmdFournisseur;
	}

	public void setIdArticle(Long idArticle) {
		this.idLigneCmdFournisseur = idArticle;
	}

	public Long getIdLigneCmdFournisseur() {
		return idLigneCmdFournisseur;
	}

	public void setIdLigneCmdFournisseur(Long idLigneCmdFournisseur) {
		this.idLigneCmdFournisseur = idLigneCmdFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
}
