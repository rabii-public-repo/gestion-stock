package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class LigneVente {

	private static final Logger log = LoggerFactory.getLogger(LigneVente.class);
	@Id
	@GeneratedValue
	private Long idLigneVente;
	@ManyToOne
	@JoinColumn(name = "idArtcile")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idVente")
	private Vente vente;

	public LigneVente() {
		log.info("+++++ bean LigneVente +++++");
	}

	public Long getIdArticle() {
		return idLigneVente;
	}

	public void setIdArticle(Long idArticle) {
		this.idLigneVente = idArticle;
	}

	public Long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
}
