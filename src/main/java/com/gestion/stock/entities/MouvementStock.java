package com.gestion.stock.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class MouvementStock {
	public static final Integer ENTREE = 1;
	public static final Integer SORTIE = 2;

	private static final Logger log = LoggerFactory.getLogger(MouvementStock.class);
	@Id
	@GeneratedValue
	private Long idMvtStck;
	private Date dateMvt;
	private BigDecimal quantite;

	public MouvementStock() {
		log.info("+++++ bean MouvementStock +++++");
	}

	public Long getIdArticle() {
		return idMvtStck;
	}

	public void setIdArticle(Long idArticle) {
		this.idMvtStck = idArticle;
	}

	public Long getIdMvtStck() {
		return idMvtStck;
	}

	public void setIdMvtStck(Long idMvtStck) {
		this.idMvtStck = idMvtStck;
	}

	public Date getDateMvt() {
		return dateMvt;
	}

	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}
}
