package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "utilisateur")
public class Utilisateur {

	private static final Logger log = LoggerFactory.getLogger(Utilisateur.class);
	@Id
	@SequenceGenerator(name = "utilisateur_seq", sequenceName = "utilisateur_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_seq")
	private Long idUtilisateur;
	private String nom;
	private String prenom;
	private String mail;
	private String motDePasse;
	private String photo;

	public Utilisateur() {
		log.info("+++++ bean utilisateur +++++");
	}

	public Long getIdArticle() {
		return idUtilisateur;
	}

	public void setIdArticle(Long idArticle) {
		this.idUtilisateur = idArticle;
	}

	public Long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
