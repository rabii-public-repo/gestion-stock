package com.gestion.stock.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class Vente {

	private static final Logger log = LoggerFactory.getLogger(Vente.class);
	@Id
	@SequenceGenerator(name = "vente_seq", sequenceName = "vente_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vente_seq")
	private Long idVente;
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	@OneToMany(mappedBy = "vente")
	private List<LigneVente> lignesVente;

	public Vente() {
		log.info("+++++ bean Vente +++++");
	}

	public Long getIdArticle() {
		return idVente;
	}

	public void setIdArticle(Long idArticle) {
		this.idVente = idArticle;
	}

	public Long getIdVente() {
		return idVente;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LigneVente> getLignesVente() {
		return lignesVente;
	}

	public void setLignesVente(List<LigneVente> lignesVente) {
		this.lignesVente = lignesVente;
	}
}
