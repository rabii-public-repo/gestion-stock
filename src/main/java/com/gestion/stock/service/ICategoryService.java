package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.Category;

public interface ICategoryService {
	public Category save(Category entity);

	public Category update(Category entity);

	public List<Category> selectAll();

	public List<Category> selectAll(String sortField, String sort);

	public Category getById(Long id);

	public Category findOne(String paramName, Object paramValue);

	public Category findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);

	public boolean delete(long id);
}
