package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.CommandeClient;

public interface ICommandeClientService {
	public CommandeClient save(CommandeClient entity);

	public CommandeClient update(CommandeClient entity);

	public List<CommandeClient> selectAll();

	public List<CommandeClient> selectAll(String sortField, String sort);

	public CommandeClient getById(Long id);

	public CommandeClient findOne(String paramName, Object paramValue);

	public CommandeClient findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);
}
