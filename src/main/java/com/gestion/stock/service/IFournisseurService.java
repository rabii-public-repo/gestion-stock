package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.Fournisseur;

public interface IFournisseurService {
	public Fournisseur save(Fournisseur entity);

	public Fournisseur update(Fournisseur entity);

	public List<Fournisseur> selectAll();

	public List<Fournisseur> selectAll(String sortField, String sort);

	public Fournisseur getById(Long id);

	public Fournisseur findOne(String paramName, Object paramValue);

	public Fournisseur findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);

	public boolean delete(long id);
}
