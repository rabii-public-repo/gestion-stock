package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {
	public LigneCommandeClient save(LigneCommandeClient entity);

	public LigneCommandeClient update(LigneCommandeClient entity);

	public List<LigneCommandeClient> selectAll();

	public List<LigneCommandeClient> selectAll(String sortField, String sort);

	public LigneCommandeClient getById(Long id);

	public LigneCommandeClient findOne(String paramName, Object paramValue);

	public LigneCommandeClient findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);
}
