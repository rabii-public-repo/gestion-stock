package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurService {
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity);

	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity);

	public List<LigneCommandeFournisseur> selectAll();

	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort);

	public LigneCommandeFournisseur getById(Long id);

	public LigneCommandeFournisseur findOne(String paramName, Object paramValue);

	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);
}
