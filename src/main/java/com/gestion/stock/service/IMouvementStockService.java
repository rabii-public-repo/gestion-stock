package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.MouvementStock;

public interface IMouvementStockService {
	public MouvementStock save(MouvementStock entity);

	public MouvementStock update(MouvementStock entity);

	public List<MouvementStock> selectAll();

	public List<MouvementStock> selectAll(String sortField, String sort);

	public MouvementStock getById(Long id);

	public MouvementStock findOne(String paramName, Object paramValue);

	public MouvementStock findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une
																				// map

	public int findCountBy(String paramName, String paramValue);

	public boolean delete(long id);
}
