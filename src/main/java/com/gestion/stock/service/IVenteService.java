package com.gestion.stock.service;

import java.util.List;

import com.gestion.stock.entities.Vente;

public interface IVenteService {
	public Vente save(Vente entity);

	public Vente update(Vente entity);

	public List<Vente> selectAll();

	public List<Vente> selectAll(String sortField, String sort);

	public Vente getById(Long id);

	public Vente findOne(String paramName, Object paramValue);

	public Vente findOne(String[] paramNames, Object[] paramvalues); // Normalement je peux la faire avec une map

	public int findCountBy(String paramName, String paramValue);

	public boolean delete(long id);
}
