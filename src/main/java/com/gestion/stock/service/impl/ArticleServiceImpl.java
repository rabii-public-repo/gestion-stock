package com.gestion.stock.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IArticleDao;
import com.gestion.stock.entities.Article;
import com.gestion.stock.service.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService {

	private static final Logger log = LoggerFactory.getLogger(ArticleServiceImpl.class);
	private IArticleDao dao;

	@Override
	public Article save(Article entity) {
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		return dao.update(entity);
	}

	@Override
	public List<Article> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Article> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Article getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public Article findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramvalues) {
		return dao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return findCountBy(paramName, paramValue);
	}

	public IArticleDao getDao() {
		return dao;
	}

	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}

	@Override
	public boolean delete(long id) {
		return this.dao.delete(id);
	}

}
