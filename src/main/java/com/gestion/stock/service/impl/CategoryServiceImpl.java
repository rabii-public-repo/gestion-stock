package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICategoryDao;
import com.gestion.stock.entities.Category;
import com.gestion.stock.service.ICategoryService;

@Transactional
public class CategoryServiceImpl implements ICategoryService {
	private ICategoryDao catDao;

	@Override
	public Category save(Category entity) {
		catDao.save(entity);
		return null;
	}

	@Override
	public Category update(Category entity) {
		catDao.update(entity);
		return null;
	}

	@Override
	public List<Category> selectAll() {
		return catDao.selectAll();
	}

	@Override
	public List<Category> selectAll(String sortField, String sort) {
		return catDao.selectAll(sortField, sort);
	}

	@Override
	public Category getById(Long id) {
		return catDao.getById(id);
	}

	@Override
	public Category findOne(String paramName, Object paramValue) {
		return catDao.findOne(paramName, paramValue);
	}

	@Override
	public Category findOne(String[] paramNames, Object[] paramvalues) {
		return catDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return catDao.findCountBy(paramName, paramValue);
	}

	public void setCatDao(ICategoryDao catDao) {
		this.catDao = catDao;
	}

	@Override
	public boolean delete(long id) {
		return this.catDao.delete(id);
	}
}
