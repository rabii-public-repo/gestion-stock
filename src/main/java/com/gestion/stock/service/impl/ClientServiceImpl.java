package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IClientDao;
import com.gestion.stock.entities.Client;
import com.gestion.stock.service.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService {
	private IClientDao cliDao;

	@Override
	public Client save(Client entity) {
		return cliDao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		return cliDao.update(entity);
	}

	@Override
	public List<Client> selectAll() {
		return cliDao.selectAll();
	}

	@Override
	public List<Client> selectAll(String sortField, String sort) {
		return cliDao.selectAll(sortField, sort);
	}

	@Override
	public Client getById(Long id) {
		return cliDao.getById(id);
	}

	@Override
	public Client findOne(String paramName, Object paramValue) {
		return cliDao.findOne(paramName, paramValue);
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramvalues) {
		return cliDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return cliDao.findCountBy(paramName, paramValue);
	}

	public void setCliDao(IClientDao cliDao) {
		this.cliDao = cliDao;
	}

	@Override
	public boolean delete(long id) {
		this.cliDao.delete(id);
		return false;
	}
}
