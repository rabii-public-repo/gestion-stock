package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICommandeClientDao;
import com.gestion.stock.entities.CommandeClient;
import com.gestion.stock.service.ICommandeClientService;

@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService {

	private ICommandeClientDao comCliDao;

	@Override
	public CommandeClient save(CommandeClient entity) {
		return comCliDao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		return comCliDao.update(entity);
	}

	@Override
	public List<CommandeClient> selectAll() {
		return comCliDao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortField, String sort) {
		return comCliDao.selectAll(sortField, sort);
	}

	@Override
	public CommandeClient getById(Long id) {
		return comCliDao.getById(id);
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		return comCliDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramvalues) {
		return comCliDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return comCliDao.findCountBy(paramName, paramValue);
	}

	public void setComCliDao(ICommandeClientDao comCliDao) {
		this.comCliDao = comCliDao;
	}
}
