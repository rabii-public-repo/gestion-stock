package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICommandeFournisseurDao;
import com.gestion.stock.entities.CommandeFournisseur;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurDao {
	private ICommandeFournisseurDao comFourDao;

	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		return comFourDao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		return comFourDao.update(entity);
	}

	@Override
	public List<CommandeFournisseur> selectAll() {
		return comFourDao.selectAll();
	}

	@Override
	public List<CommandeFournisseur> selectAll(String sortField, String sort) {
		return comFourDao.selectAll(sortField, sort);
	}

	@Override
	public CommandeFournisseur getById(Long id) {
		return comFourDao.getById(id);
	}

	@Override
	public CommandeFournisseur findOne(String paramName, Object paramValue) {
		return comFourDao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramvalues) {
		return comFourDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return comFourDao.findCountBy(paramName, paramValue);
	}

	public void setComFourDao(ICommandeFournisseurDao comFourDao) {
		this.comFourDao = comFourDao;
	}

	@Override
	public boolean delete(long id) {
		// TODO Auto-generated method stub
		return false;
	}

}
