package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IFournisseurDao;
import com.gestion.stock.entities.Fournisseur;
import com.gestion.stock.service.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService {
	private IFournisseurDao fourDao;

	@Override
	public Fournisseur save(Fournisseur entity) {
		return fourDao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		return fourDao.update(entity);
	}

	@Override
	public List<Fournisseur> selectAll() {
		return fourDao.selectAll();
	}

	@Override
	public List<Fournisseur> selectAll(String sortField, String sort) {
		return fourDao.selectAll(sortField, sort);
	}

	@Override
	public Fournisseur getById(Long id) {
		return fourDao.getById(id);
	}

	@Override
	public Fournisseur findOne(String paramName, Object paramValue) {
		return fourDao.findOne(paramName, paramValue);
	}

	@Override
	public Fournisseur findOne(String[] paramNames, Object[] paramvalues) {
		return fourDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return fourDao.findCountBy(paramName, paramValue);
	}

	public void setFourDao(IFournisseurDao fourDao) {
		this.fourDao = fourDao;
	}

	@Override
	public boolean delete(long id) {
		return this.fourDao.delete(id);
	}
}
