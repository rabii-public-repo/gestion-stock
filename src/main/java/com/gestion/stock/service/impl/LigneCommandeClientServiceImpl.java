package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneCommandeClientDao;
import com.gestion.stock.entities.LigneCommandeClient;
import com.gestion.stock.service.ILigneCommandeClientService;

@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService {
	private ILigneCommandeClientDao ligneComCliDao;

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		return ligneComCliDao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		return ligneComCliDao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		return ligneComCliDao.selectAll();
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortField, String sort) {
		return ligneComCliDao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeClient getById(Long id) {
		return ligneComCliDao.getById(id);
	}

	@Override
	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		return ligneComCliDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramvalues) {
		return ligneComCliDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return ligneComCliDao.findCountBy(paramName, paramValue);
	}

	public void setLigneComCliDao(ILigneCommandeClientDao ligneComCliDao) {
		this.ligneComCliDao = ligneComCliDao;
	}
}
