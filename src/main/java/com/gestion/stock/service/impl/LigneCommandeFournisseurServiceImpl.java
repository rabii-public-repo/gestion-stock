package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneCommandeFournisseurDao;
import com.gestion.stock.entities.LigneCommandeFournisseur;
import com.gestion.stock.service.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService {
	private ILigneCommandeFournisseurDao ligneComFourDao;

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		return ligneComFourDao.save(entity);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		return ligneComFourDao.update(entity);
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		return ligneComFourDao.selectAll();
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		return ligneComFourDao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur getById(Long id) {
		return ligneComFourDao.getById(id);
	}

	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		return ligneComFourDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramvalues) {
		return ligneComFourDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return ligneComFourDao.findCountBy(paramName, paramValue);
	}

	public void setLigneComFourDao(ILigneCommandeFournisseurDao ligneComFourDao) {
		this.ligneComFourDao = ligneComFourDao;
	}
}
