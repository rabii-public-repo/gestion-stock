package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneVenteDao;
import com.gestion.stock.entities.LigneVente;
import com.gestion.stock.service.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService {
	private ILigneVenteDao ligneVenDao;

	@Override
	public LigneVente save(LigneVente entity) {
		return ligneVenDao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		return ligneVenDao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {
		return ligneVenDao.selectAll();
	}

	@Override
	public List<LigneVente> selectAll(String sortField, String sort) {
		return ligneVenDao.selectAll(sortField, sort);
	}

	@Override
	public LigneVente getById(Long id) {
		return ligneVenDao.getById(id);
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {
		return ligneVenDao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramNames, Object[] paramvalues) {
		return ligneVenDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return ligneVenDao.findCountBy(paramName, paramValue);
	}

	public void setLigneVenDao(ILigneVenteDao ligneVenDao) {
		this.ligneVenDao = ligneVenDao;
	}
}
