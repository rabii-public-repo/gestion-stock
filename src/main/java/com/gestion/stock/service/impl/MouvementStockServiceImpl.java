package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IMouvementStockDao;
import com.gestion.stock.entities.MouvementStock;
import com.gestion.stock.service.IMouvementStockService;

@Transactional
public class MouvementStockServiceImpl implements IMouvementStockService {
	private IMouvementStockDao mouvStkDao;

	@Override
	public MouvementStock save(MouvementStock entity) {
		return mouvStkDao.save(entity);
	}

	@Override
	public MouvementStock update(MouvementStock entity) {
		return mouvStkDao.update(entity);
	}

	@Override
	public List<MouvementStock> selectAll() {
		return mouvStkDao.selectAll();
	}

	@Override
	public List<MouvementStock> selectAll(String sortField, String sort) {
		return mouvStkDao.selectAll(sortField, sort);
	}

	@Override
	public MouvementStock getById(Long id) {
		return mouvStkDao.getById(id);
	}

	@Override
	public MouvementStock findOne(String paramName, Object paramValue) {
		return mouvStkDao.findOne(paramName, paramValue);
	}

	@Override
	public MouvementStock findOne(String[] paramNames, Object[] paramvalues) {
		return mouvStkDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return mouvStkDao.findCountBy(paramName, paramValue);
	}

	public void setMouvStkDao(IMouvementStockDao mouvStkDao) {
		this.mouvStkDao = mouvStkDao;
	}

	@Override
	public boolean delete(long id) {
		return this.mouvStkDao.delete(id);
	}

}
