package com.gestion.stock.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IUtilisateurDao;
import com.gestion.stock.entities.Utilisateur;
import com.gestion.stock.service.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {
	private IUtilisateurDao utilDao;

	@Override
	public Utilisateur save(Utilisateur entity) {
		return utilDao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		return utilDao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {
		return utilDao.selectAll();
	}

	@Override
	public List<Utilisateur> selectAll(String sortField, String sort) {
		return utilDao.selectAll(sortField, sort);
	}

	@Override
	public Utilisateur getById(Long id) {
		return utilDao.getById(id);
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {
		return utilDao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramvalues) {
		return utilDao.findOne(paramNames, paramvalues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return utilDao.findCountBy(paramName, paramValue);
	}

	public void setUtilDao(IUtilisateurDao utilDao) {
		this.utilDao = utilDao;
	}

	@Override
	public boolean delete(long id) {

		return this.utilDao.delete(id);
	}
}
