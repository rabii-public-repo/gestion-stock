<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>

<title><fmt:message key="common.clients.new"></fmt:message></title>
<%@ include file="/WEB-INF/views/includes/header.jsp"%>

</head>

<body id="page-top">
	<div id="wrapper">
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>
				<div class="container-fluid">

					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">
								<fmt:message key="common.update.client"></fmt:message>
							</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<div id="dataTable_wrapper"
									class="dataTables_wrapper dt-bootstrap4">

									<form:form method="POST" action="/clients/update"
										modelAttribute="client">
										<div class="form-row">

											<div class="form-group col-md-6">
												<form:label path="nom">
													<fmt:message key="client.name"></fmt:message>
												</form:label>
												<form:input path="nom" class="form-control"
													value="${client.nom}" />
												 
												 <input type="hidden" id="idClient" 
												 		name="idClient" value="${client.idClient}">
													
											</div>

											<div class="form-group col-md-6">
												<form:label path="prenom">
													<fmt:message key="client.firstname"></fmt:message>
												</form:label>
												<form:input path="prenom" class="form-control"
													value="${client.prenom}" />
											</div>

											<div class="form-group col-md-6">
												<form:label path="email">
													<fmt:message key="client.email"></fmt:message>
												</form:label>
												<form:input path="email" class="form-control" />
											</div>


											<div class="form-group col-md-6">
												<form:label path="adresse">
													<fmt:message key="client.adress"></fmt:message>
												</form:label>
												<form:input path="adresse" class="form-control"
													value="${client.adresse}" />
											</div>

										</div>
										<br>
										<button type="submit" class="btn btn-primary">
											<fmt:message key="client.button.update"></fmt:message>
										</button>
									</form:form>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<%@ include file="/WEB-INF/views/includes/footer.jsp"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/includes/logout-modal.jsp"%>
	<%@ include file="/WEB-INF/views/includes/script.jsp"%>
</body>

</html>
