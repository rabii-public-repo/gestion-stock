<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true"%>
<%@ page trimDirectiveWhitespaces="true"%>

<%
	String locale = "fr_FR";
%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="com.stock.mvc.i18n.applicationresources" />