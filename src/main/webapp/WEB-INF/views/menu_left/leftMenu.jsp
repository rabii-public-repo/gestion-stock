<c:url value="/" var="home" />
<c:url value="/articles" var="articles" />	
<c:url value="/clients" var="clients" />
<c:url value="/commandesclients" var="commandesclients" />
<c:url value="/fournisseurs" var="fournisseurs" />
<c:url value="/commandesfournisseurs" var="commandesfournisseurs" />
<c:url value="/utilisateurs" var="utilisateurUrl" />
<c:url value="/categories" var="categorie" />
<c:url value="/mouvementStocks" var="stock" />
<c:url value="/ventes" var="venteURL" />
<ul
	class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
	id="accordionSidebar">

	<!-- application name -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center"
		href="${home}">
		
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		
		<div class="sidebar-brand-text mx-3">
			<fmt:message key="application.name"></fmt:message>
			<sup>2</sup>
		</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- tableau de bord -->
	<li class="nav-item">
		<a class="nav-link" href="${home}"> 
				<i class="fas fa-fw fa-tachometer-alt" ></i>
					<span>
						<fmt:message key="common.dashbord"></fmt:message>
					</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">Interface</div>
	
	<!--	Articles	-->
	<li class="nav-item">
		<a class="nav-link" href="${articles}"> 
			<i class="fas fa-fw fa-table"></i> 
				<span><fmt:message key="common.articles"></fmt:message></span>
		</a>
	</li>

	<!-- Clients -->
		<li class="nav-item">
			<a class="nav-link collapsed" href="${clients}" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
				aria-controls="collapseTwo"> 
					<i class="fas fa-fw fa-cog"></i> 
						<span>
							<fmt:message key="common.clients"></fmt:message>
						</span>
			</a>
			
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"	data-parent="#accordionSidebar">
				
				<div class="bg-white py-2 collapse-inner rounded">
			
					<h6 class="collapse-header">
						<fmt:message key="common.clients"></fmt:message>
					</h6>
					<a class="collapse-item" href="${clients}">
						<fmt:message key="common.clients"></fmt:message>
					</a> 
					<a class="collapse-item" href="${commandesclients}">
						<fmt:message key="common.commandes.clients"></fmt:message>
					</a>
				
				</div>
			
			</div>
		</li>

	<!-- fournisseurs -->
	<li class="nav-item">
	  <a class="nav-link" href="${fournisseurs}" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"aria-controls="collapsePages">
	    <i class="fas fa-fw fa-folder"></i>
	      	<span>
		  		<fmt:message key="common.fournisseurs"></fmt:message>
	    	</span>
	    </a>
	
	    <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
	    
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">
					<fmt:message key="common.fournisseurs"></fmt:message>
				</h6>
				<a class="collapse-item" href="${fournisseurs}">
					<fmt:message key="common.fournisseurs"></fmt:message>
				</a>
				<a class="collapse-item" href="${commandesfournisseurs}">
					<fmt:message key="common.commandes.fournisseurs"></fmt:message>
				</a>
			</div>
	    </div>
	</li>

	<!--stock-->
	<li class="nav-item">
		<a class="nav-link" href="${stock}"> 
			<i class="fas fa-fw fa-table"></i> 
				<span>
					<fmt:message key="common.stock"></fmt:message>
				</span>
		</a>
	</li>
	
	<!--venteURL -->
	<li class="nav-item">
		<a class="nav-link" href="${venteURL}"> 
			<i class="fas fa-fw fa-table"></i> 
				<span>
					<fmt:message key="common.vente"></fmt:message>
				</span>
		</a>
	</li>
	
	<!-- parametrage -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>
				<fmt:message key="common.parametrage"></fmt:message>
		  </span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><fmt:message key="common.parametrage"></fmt:message></h6>
            <a class="collapse-item" href="${utilisateurUrl}">
				<fmt:message key="common.parametrage.utilisateur"></fmt:message>
			</a>
            <a class="collapse-item" href="${categorie}">
				<fmt:message key="common.parametrage.category"></fmt:message>
			</a>
          </div>
        </div>
      </li>
	
</ul>