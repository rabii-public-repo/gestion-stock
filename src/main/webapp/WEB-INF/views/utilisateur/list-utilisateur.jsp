<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><fmt:message key="common.liste.utilisateurs"></fmt:message></h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_length" id="dataTable_length">
							<label>
								<select name="dataTable_length" aria-controls="dataTable"
									class="custom-select custom-select-sm form-control form-control-sm">
									<option value="10">10</option>
									<option value="25">25</option>
								</select>
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div id="dataTable_filter" class="dataTables_filter">
							<label>Search:<input type="search"
								class="form-control form-control-sm" placeholder=""
								aria-controls="dataTable">
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<!-- button -->
						<a href="<c:url value = "/utilisateurs/new"/>" class="btn btn-primary">
							<fmt:message key="utilisateur.button.add"></fmt:message>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-bordered dataTable" id="dataTable"
							width="100%" cellspacing="0" role="grid"
							aria-describedby="dataTable_info" style="width: 100%;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1" aria-sort="ascending"
										aria-label="Name: activate to sort column descending"
										style="width: 58px;">
										<fmt:message key="utilisateur.name"></fmt:message>
									</th>
									<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Position: activate to sort column ascending"
										style="width: 62px;">
										<fmt:message key="utilisateur.firstname"></fmt:message>
									</th>
									<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Office: activate to sort column ascending"
										style="width: 50px;">
										<fmt:message key="utilisateur.mail"></fmt:message>
									</th>
									<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Age: activate to sort column ascending"
										style="width: 31px;">
										<fmt:message key="utilisateur.password"></fmt:message>
									</th>
										<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Age: activate to sort column ascending"
										style="width: 31px;">
										#
									</th>
							</thead>
							<tbody>
								<c:forEach items="${utilisateursList}" var="utilisateur">
									<form:form method="POST" action="/utilisateurs/delete/${utilisateur.idUtilisateur}" modelAttribute="utilisateur">
										<tr role="row" class="odd">
											<td class="sorting_1">
												<a href = "<c:url value = "/utilisateurs/update?id=${utilisateur.idUtilisateur}"/>">
												${utilisateur.nom}
											</a>
										</td>
										
										<td>${utilisateur.prenom}</td>
										
										<td>${utilisateur.mail}</td>
										
										<td>${utilisateur.motDePasse}</td>
		
										<td>
											<button type="submit" class="btn btn-danger">
												<fmt:message key="utilisateur.button.delete"></fmt:message>
											</button>
										</td>
										</tr>	
									</form:form> 								
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
					<%@ include file="/WEB-INF/views/includes/pagination.jsp"%>
			</div>
		</div>
	</div>
</div>
