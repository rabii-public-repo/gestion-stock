<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><fmt:message key="common.utilisateur"></fmt:message></h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<div class="row">
					<div class="col-sm-12 col-md-6">
					</div>
					<div class="col-sm-12 col-md-6">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<table class="table table-bordered dataTable" id="dataTable"
							width="100%" cellspacing="0" role="grid"
							aria-describedby="dataTable_info" style="width: 100%;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1" aria-sort="ascending"
										aria-label="Name: activate to sort column descending"
										style="width: 58px;">
										<fmt:message key="utilisateur.name"></fmt:message>
									</th>
									<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Position: activate to sort column ascending"
										style="width: 62px;">
										<fmt:message key="utilisateur.firstname"></fmt:message>
									</th>
									<th class="sorting" tabindex="0" aria-controls="dataTable"
										rowspan="1" colspan="1"
										aria-label="Office: activate to sort column ascending"
										style="width: 50px;">
										<fmt:message key="utilisateur.email"></fmt:message>
									</th>
							</thead>
							<tbody>
								    <tr role="row" class="odd">
										<td class="sorting_1">${utilisateur.nom}</td>
										<td>${utilisateur.prenom}</td>
										<td>${utilisateur.email}</td>
									</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
