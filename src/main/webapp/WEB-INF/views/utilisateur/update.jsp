<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>

<title><fmt:message key="common.utilisateurs.new"></fmt:message></title>
<%@ include file="/WEB-INF/views/includes/header.jsp"%>

</head>

<body id="page-top">
	<div id="wrapper">
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>
				<div class="container-fluid">

					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">
								<fmt:message key="common.update.utilisateur"></fmt:message>
							</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<div id="dataTable_wrapper"
									class="dataTables_wrapper dt-bootstrap4">

									<form:form method="POST" action="/utilisateurs/update"
										modelAttribute="utilisateur">
										<div class="form-row">
										<div class="form-group col-md-6">
											<form:label path="nom">
												<fmt:message key="utilisateur.name"></fmt:message>
											</form:label>
										
											<form:input path="nom" class="form-control"
												value="${utilisateur.nom}" />
										
											 <input type="hidden" id="idUtilisateur" 
										 		name="idUtilisateur" value="${utilisateur.idUtilisateur}">
										</div>
										
										
										<div class="form-group col-md-6">
											<form:label path="prenom">
												<fmt:message key="utilisateur.firstname"></fmt:message>
											</form:label>
											<form:input path="prenom" class="form-control"
												value="${utilisateur.prenom}" />
										</div>
										
										<div class="form-group col-md-6">
											<form:label path="mail">
												<fmt:message key="utilisateur.mail"></fmt:message>
											</form:label>
											<form:input path="mail" class="form-control" 
												value="${utilisateur.mail}" />
										</div>
										
										<div class="form-group col-md-6">
											<form:label path="motDePasse">
												<fmt:message key="utilisateur.password"></fmt:message>
											</form:label>
											<form:input path="motDePasse" class="form-control" 
												value="${utilisateur.motDePasse}" />
										</div>


										</div>
										<br>
										<button type="submit" class="btn btn-primary">
											<fmt:message key="utilisateur.button.update"></fmt:message>
										</button>
									</form:form>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<%@ include file="/WEB-INF/views/includes/footer.jsp"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/includes/logout-modal.jsp"%>
	<%@ include file="/WEB-INF/views/includes/script.jsp"%>
</body>

</html>
