<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>

<title><fmt:message key="common.ventes.new"></fmt:message></title>
<%@ include file="/WEB-INF/views/includes/header.jsp"%>

</head>

<body id="page-top">
	<div id="wrapper">
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>
				<div class="container-fluid">

					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">
								<fmt:message key="common.update.vente"></fmt:message>
							</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<div id="dataTable_wrapper"
									class="dataTables_wrapper dt-bootstrap4">

									<form:form method="POST" action="/ventes/update"
										modelAttribute="vente">
										<div class="form-row">

											<div class="form-group col-md-6">
												<form:label path="code">
													<fmt:message key="vente.code"></fmt:message>
												</form:label>
												<form:input path="code" class="form-control"
													value="${vente.code}" />
												 
												 <input type="hidden" id="idVente" 
												 		name="idVente" value="${vente.idVente}">
													
											</div>

											<div class="form-group col-md-6">
												<form:label path="dateVente">
													<fmt:message key="vente.dateVente"></fmt:message>
												</form:label>
												<form:input path="dateVente" class="form-control"
													value="${vente.dateVente}" />
											</div>

										</div>
										<br>
										<button type="submit" class="btn btn-primary">
											<fmt:message key="vente.button.update"></fmt:message>
										</button>
									</form:form>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<%@ include file="/WEB-INF/views/includes/footer.jsp"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/includes/logout-modal.jsp"%>
	<%@ include file="/WEB-INF/views/includes/script.jsp"%>
</body>

</html>
