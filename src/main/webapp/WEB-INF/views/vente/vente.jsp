<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>


<title>	<fmt:message key="common.ventes"></fmt:message>  </title>
<%@ include file="/WEB-INF/views/includes/header.jsp"%>

</head>

<body id="page-top">
	<div id="wrapper">
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>
				<div class="container-fluid">
					<h1 class="h3 mb-4 text-gray-800">
						<fmt:message key="common.ventes"></fmt:message>
					</h1>
				
<%-- 					<c:if test="${empty ventesList}"> --%>
<%--     					<%@ include file="one-vente.jsp"%> --%>
<%-- 					</c:if> --%>
<%-- 					<c:if test="${not empty ventesList}"> --%>
  						<%@ include file="list-vente.jsp"%>
<%-- 					</c:if> --%>
					
				</div>
			</div>
			<%@ include file="/WEB-INF/views/includes/footer.jsp"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/includes/logout-modal.jsp"%>
	<%@ include file="/WEB-INF/views/includes/script.jsp"%>
</body>

</html>
